# Node-SISE Project

This project is an implementation of the SISE model developed by Dr. Russell Thackston.

The platform that this project is being developed under is Node.js, so you will need to have Node.js installed on your computer to run the code.  For more information about Node.js and how to install, go to:  [http://www.nodejs.org](http://www.nodejs.org).

Once you have Node.js installed, navigate to the project's root directory within your command prompt or terminal and do the following command:

	npm install

This will install all of the modules associated with this node application.  Once completed, run the following command:

	node app
	
This will automatically run the Node.js server on localhost:3000.  Navigate your web browser to http://localhost:3000 and you will see the website.