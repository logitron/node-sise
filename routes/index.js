var clone = require('clone');

/*
 * GET home page.
 */
ActivityProvider = require('../activityProvider').ActivityProvider;
var activityProvider = new ActivityProvider();

exports.index = function(req, res) {
  	activityProvider.findAll(function(error, docs) { 
		res.render('index.jade', {
			title: 'Activities',
			activities: docs
		});
	});
};

exports.view_add_activity = function(req, res) {
	activityProvider.findAll(function(error, docs) {
		sortResults(docs, 'timeActual', true); 
		res.render('add-activity.jade', {
			title: 'Add Activity',
			activities: docs
		});
	});
};

exports.add_activity = function(req, res) {
	var activity = {
		"title": req.body.txtTitle,
		"description": req.body.txtDescription,
		"timeEstimate": {
			"lower": req.body.estLower,
			"upper": req.body.estUpper
		},
		"timeActual": null,
		"error": null
	}
	
	activityProvider.findAll(function(error, docs) {
		var shiftBias = getShiftBias(docs);
		
		if (shiftBias.shift != 0) {
			for (doc in docs)
				applyShiftBias(doc, shiftBias);
		}
		
		res.render('width-bias.jade', {
			title: 'Width Bias',
			activity: activity,
			activities: docs,
			shift: shiftBias.shift,
			hitRate: shiftBias.hitRate
		});
	});
};

// Gets the shift bias and hit rate for the activities in the following format:
// { 'shift': 0, 'hitRate': 0 }
function getShiftBias(activities) {
	var errors = 0;
	
	var avgLower = 0;
	var avgUpper = 0;
	var avgActual = 0;
	var avgCount = 0;
	
	console.log(activities);
	
	// Loop through activities to sum up lower/upper/actual times
	for (var i = 0; i < activities.length; i++) {
		var activity = activities[i];
		
		if (activity.error != 0)
			errors++;
			
		if (activity.timeEstimate.upper != -1) {
			avgLower = activity.timeEstimate.lower;
			avgUpper = activity.timeEstimate.upper;
			avgActual = activity.timeActual;
			avgCount++;
		}
	}
	
	// Calculate hit rate before applying shift bias
	var hitRate = (activities.length - errors) / activities.length;
	
	// Calculate averages for lower/upper/actual times
	avgLower /= avgCount;
	avgUpper /= avgCount;
	avgActual /= avgCount;
	
	// Calculate shift bias
	var midPoint = ((avgUpper - avgLower) / 2) + avgLower;
	var shiftBias = Math.floor(avgActual - midPoint);
	
	// Deep copy activities and apply shift bias
	var shiftedActivities = clone(activities);
	errors = 0;
	for (var i = 0; i < shiftedActivities.length; i++) {
		var activity = shiftedActivities[i];
		
		if (applyShiftBias(activity, shiftBias) > 0)
			errors++;
	}
	
	// Calculate hit rate after applying shift bias
	var shiftedHitRate = (shiftedActivities.length - errors) / shiftedActivities.length;
	
	// If hit rate is better after shift bias, return shifted 
	// activities; otherwise, return original activities
	var result = (shiftedHitRate > hitRate) ? shift : 0;
	
	return { 'shift': result, 'hitRate': hitRate };
}

// Applies shift bias to the activity and returns the new error
function applyShiftBias(activity, shiftBias) {
	activity.timeEstimate.lower += shiftBias;
	activity.timeEstimate.upper += shiftBias;
	
	if (activity.timeActual < activity.timeEstimate.lower)
		activity.error = activity.timeEstimate.lower - activity.timeActual;
	else if (activity.timeActual > activity.timeEstimate.upper)
		activity.error = activity.timeActual - activity.timeEstimate.lower;
	else
		activity.error = 0;
		
	return activity.error;
}

function sortResults(docs, prop, asc) {
    docs = docs.sort(function(a, b) {
        if (asc) return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
        else return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
    });
}