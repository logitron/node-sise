var activityCounter = 1;

ActivityProvider = function() {};
ActivityProvider.prototype.dummyData = [];

ActivityProvider.prototype.findAll = function(callback) {
	callback(null, this.dummyData);
};

ActivityProvider.prototype.findById = function(id, callback) {
	var result = null;
	
	for (var i = 0; i < this.dummyData.length; i++) {
		if (this.dummyData[i]._id == id) {
			result = this.dummyData[i];
			break;
		}
	}
	
	callback(null, result);
};

ActivityProvider.prototype.save = function(activities, callback) {
	var activity = null;
	
	if (typeof(activities.length) == "undefined")
		activities = [activities];
		
	for (var i = 0; i < activities.length; i++) {
		activity = activities[i];
		activity._id = activityCounter++;
		activity.created_at = new Date();
		
		this.dummyData[this.dummyData.length] = activity;
	}
	
	callback(null, activities);
};

/* Bootstrap dummy data */
new ActivityProvider().save([
	{title: 'Design FAQ data model', description: '', timeEstimate: {lower: 0, upper: 4}, timeActual: 2, error: 0},
	{title: 'Create FAQ classes', description: '', timeEstimate: {lower: 2, upper: 6}, timeActual: 2, error: 0},
	{title: 'Create security classes', description: '', timeEstimate: {lower: 2, upper: 8}, timeActual: 3, error: 0},
	{title: 'Create user classes', description: '', timeEstimate: {lower: 5, upper: 8}, timeActual: 4, error: 1},
	{title: 'Create DB tables in MySQL', description: '', timeEstimate: {lower: 0, upper: 6}, timeActual: 5, error: 0},
	{title: 'Design DB tables', description: '', timeEstimate: {lower: 0, upper: 11}, timeActual: 6, error: 0},
	{title: 'Design user model', description: '', timeEstimate: {lower: 0, upper: 10}, timeActual: 8, error: 0},
	{title: 'Design security model', description: '', timeEstimate: {lower: 0, upper: -1}, timeActual: 10, error: 0},
	{title: 'Design content model', description: '', timeEstimate: {lower: 8, upper: 10}, timeActual: 11, error: 1},
	{title: 'Create data connector classes', description: '', timeEstimate: {lower: 0, upper: 11}, timeActual: 14, error: 3}
], function(error, activities) {});

exports.ActivityProvider = ActivityProvider;